package co.simplon.p14.springjwt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.bind.annotation.CrossOrigin;

import co.simplon.p14.springjwt.security.AuthFilter;
import co.simplon.p14.springjwt.security.JwtFilter;
import co.simplon.p14.springjwt.services.AuthService;

@Configuration
@EnableWebSecurity
@CrossOrigin(origins = "*")
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private AuthService authService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http //Pas ouf, mais facile à implémenter
        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and().authorizeRequests()
        //Ici c'est là qu'on dit à notre spring security quelle route est protégée ou pas (et comment)
        .mvcMatchers("/api/account/**").authenticated()
        .mvcMatchers("/api/admin/**").hasRole("ADMIN")
        .anyRequest().permitAll()
        .and().csrf().disable()
        .addFilter(new AuthFilter(super.authenticationManager()))
        .addFilterBefore(new JwtFilter(), UsernamePasswordAuthenticationFilter.class);
        
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(authService)
            .passwordEncoder(passwordEncoder());
    }

    


    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(10);
    }
}
