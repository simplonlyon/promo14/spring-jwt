package co.simplon.p14.springjwt.controller;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.simplon.p14.springjwt.entity.User;

@RestController
public class AccountController {
    
    @GetMapping("/api/account")
    public User account(@AuthenticationPrincipal User user) {
        return user;
    }
}
