package co.simplon.p14.springjwt.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.p14.springjwt.entity.User;
import co.simplon.p14.springjwt.repository.UserRepository;

@RestController
@RequestMapping("/api/user")
public class AuthController {
    @Autowired
    private UserRepository repo;
    @Autowired
    private PasswordEncoder encoder;

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public User register(@Valid @RequestBody User user) {

        if(repo.findByEmail(user.getEmail()).isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User Already Exists");
        }

        String encoded = encoder.encode(user.getPassword());
        user.setPassword(encoded);
        user.setRole("ROLE_USER");
        repo.save(user);
        return user;
    }

}
